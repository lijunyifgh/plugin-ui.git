const modulesFiles = require.context('./myTemplate', false, /\.vue$/);
let $request = modulesFiles.keys().reduce((add, m_path) => {
  let objKey = m_path.replace(/\.\/([^]*?)\.vue/g, '$1')
  add[objKey] = {};
  let require_content = modulesFiles(m_path);
  for (const key in require_content) {
    if (require_content.hasOwnProperty(key)) {
      let element = require_content[key];
      add[objKey] = element;
    }
  }
  return add;
}, {});



$request.install = function install(Vue) {
  for (const key in $request) {
    if ($request.hasOwnProperty(key)) {
      const element = $request[key];
      Vue.component(key, element);
    }
  }
}
export default $request
